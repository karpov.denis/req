
const onUploadProg = e => {
  // eslint-disable-next-line no-undef
  graphBar(e);
  // eslint-disable-next-line no-undef
  procentBar(e);
};

const uploadForm = document.getElementById('uploadForm');
const downloadForm = document.getElementById('downloadForm');

const activeBtn = (inputClassName, btn) => () => {
  const button = document.querySelector(btn);
  const input = document.querySelector(inputClassName);

  if (input.value !== null) {
    button.classList.remove('btn--disable');
    button.disabled = 0;
  }
};
// eslint-disable-next-line no-undef
window.onload = getList('http://localhost:8000', '/list')
  .then(data => {
    const container = document.querySelector('.listImg-container');

    // eslint-disable-next-line no-undef
    container.insertAdjacentHTML('beforeend', renderList(data));
  })
  .then(() => {
    const items = document.getElementsByClassName('filesList')[0];
    items.addEventListener('click', e => {
      // eslint-disable-next-line no-undef
      sendListName(e.target.innerHTML, 'downloadFile');
    });
  });


document.querySelector('.downloadFile').addEventListener('focus', activeBtn('.downloadFile', '.btn--download'));
document.querySelector('.btn--choose').addEventListener('click', activeBtn('.btn--choose', '.btn--upload'));

uploadForm.onsubmit = function(e) {
  e.preventDefault();
  const form = new FormData();
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'multipart/form-data');
  form.append('sampleFile', e.target.sampleFile.files[0]);


  // eslint-disable-next-line no-undef
  const req = new HttpRequest({
    baseUrl: 'http://localhost:8000',
    baseHeaders: myHeaders
  });
  req.post('/upload', {
    data: form,
    onUploadProgress: onUploadProg
  }).then(() => {
    // eslint-disable-next-line no-undef
    getList('http://localhost:8000', '/list')
      .then(data => {
        const container = document.querySelector('.listImg-container');
        // eslint-disable-next-line no-undef
        container.insertAdjacentHTML('beforeend', renderList(data));
      })
      .then(() => {
        const items = document.getElementsByClassName('filesList')[0];
        items.addEventListener('click', e => {
          // eslint-disable-next-line no-undef
          sendListName(e.target.innerHTML, 'downloadFile');
        });
      });
  });
};

downloadForm.onsubmit = function(e) {
  e.preventDefault();
  const form = new FormData();
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'multipart/form-data');

  // eslint-disable-next-line no-undef
  const req = new HttpRequest({
    baseUrl: 'http://localhost:8000',
    baseHeaders: myHeaders
  });


  const fileName = e.target[0].value;
  // eslint-disable-next-line no-undef
  req.get(`/files/${fileName}`, { responseType: 'blob', onDownloadProgress: graphBar })
    // eslint-disable-next-line no-undef
    .then(data => getFile({
      data,
      fileName,
      // eslint-disable-next-line no-undef
      'callBack': renderImgOnPage,
      'dataType': 'image'
    }));
};


