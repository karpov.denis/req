const renderImgOnPage = file => {
  const imgContainer = document.createElement('div');
  const img = document.createElement('img');
  img.src = file;
  img.style.margin = '0 auto';
  img.style.width = '50%';
  imgContainer.className = 'imgContainer';
  imgContainer.appendChild(img);
  const contContainer = document.querySelector('.listImg-container');
  contContainer.appendChild(imgContainer);
};

const getFile = param => {
  const { data, fileName, dataType, callBack } = param;
  const file = window.URL.createObjectURL(data);
  const a = document.createElement('a');
  a.href = file;
  a.download = fileName;


  if (data.type.includes(dataType)) {
    callBack(file);
  } else {
    a.click();
  }
};


const graphBar = e => {
  const bar = document.querySelector('.progBar');
  bar.max = e.total;
  bar.value = e.loaded;
};

const procentBar = e => {
  const barNumber = document.querySelector('.barNumber');
  barNumber.innerHTML = `${Math.round(e.loaded * 100 / e.total)}%`;
};


