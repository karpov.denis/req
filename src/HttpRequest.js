class HttpRequest {
  constructor({ baseUrl, headers }) {
    this.baseUrl = baseUrl;
    this.headers = headers;
  }

  #getFullUrl = (url, params) => {
    const fullUrl = new URL(url, this.baseUrl);

    for (const key in params) {
      fullUrl.searchParams.set(key, params[key]);
    }

    return fullUrl;
  }

  #xhrPromise = (method, url, config) => {
    const {
      transformResponse = [],
      params,
      responseType = 'json',
      data,
      onUploadProgress,
      onDownloadProgress
    } = config;

    let { headers } = config;
    headers = { ...this.headers, ...headers };
    const fullUrl = this.#getFullUrl(url, params);

    const xhr = new XMLHttpRequest();
    xhr.open(method, fullUrl);

    xhr.responseType = responseType;

    for (const key in headers) {
      xhr.setRequestHeader(key, headers[key]);
    }

    xhr.onprogress = onDownloadProgress;
    xhr.upload.onprogress = onUploadProgress;


    return new Promise((res, rej) => {
      xhr.onload = () => {
        if (xhr.status !== 200) {
          const err = new Error(`${xhr.status}: ${xhr.statusText}; description: response error`);

          return rej(err);
        }


        const responseData = transformResponse.reduce((accum, fn) => fn(accum), xhr.response);

        return res(responseData);
      };

      xhr.onerror = () => {
        const err = new Error(`${xhr.status}: ${xhr.statusText}; description: load error`);

        return rej(err);
      };

      xhr.send(data);
    });
  };


  get(url, config) {
    return this.#xhrPromise('GET', url, config);
  }

  post(url, config) {
    return this.#xhrPromise('POST', url, config);
  }
}
