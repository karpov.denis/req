const getList = (url, getUrl) => {
  // eslint-disable-next-line no-undef
  const req = new HttpRequest({
    baseUrl: url
  });

  return req.get(getUrl, {});
};

const renderList = data => {
  const filesList = document.querySelector('.filesList');

  if (filesList) {
    filesList.remove();
  }

  const e = `
    <ul class='filesList'>
      ${data.length
    ? data.map(el => `<li class='filesList-itm'>${el}</li>`).join('\n')
    : 'List is empty!'}
    </ul>
  `;

  return e;
};

const clearList = className => {
  document.querySelector(className).remove();
};

const sendListName = (fileName, inputName) => {
  const input = document.getElementsByName(inputName)[0];
  input.value = fileName;
};

